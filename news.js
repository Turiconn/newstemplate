function openSideNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}

var header = document.getElementById("myNav");
var link = header.getElementsByClassName("navActive");
for (var i = 0; i < link.length; i++) {
    link[i].addEventListener("click", function () {
        var current = document.getElementsByClassName("active_link");
        current[0].className = current[0].className.replace(" active_link", "");
        this.className += " active_link";
    });
}

const divs = document.querySelectorAll(".area2_slider");
let index = 0;
function slider(direction) {
    if (direction === "left") {
        index--;
        if (index < 0) {
            index = divs.length - 1;
        }
    } else {
        index++;
        if (index > divs.length - 1) {
            index = 0;
        }
    }
    document.getElementsByClassName("active_slider")[0].classList.remove("active_slider");
    divs[index].classList.add("active_slider");
}


/* 
this javascript is only to change the "actpage" attribut on the .cdp div
*/

window.onload = function () {
    var paginationPage = parseInt($('.cdp').attr('actpage'), 10);
    $('.cdp_i').on('click', function () {
        var go = $(this).attr('href').replace('#!', '');
        if (go === '+1') {
            paginationPage++;
        } else if (go === '-1') {
            paginationPage--;
        } else {
            paginationPage = parseInt(go, 10);
        }
        $('.cdp').attr('actpage', paginationPage);
    });
};

// Navbar scroll top 100%
window.onscroll = function () { myFunction() };
var navbar = document.getElementById("my_navbar");
var sticky = navbar.offsetTop;

function myFunction() {
    if (window.pageYOffset >= sticky) {
        navbar.classList.add("fixed")

    }
    else {
        navbar.classList.remove("fixed");


    }
}